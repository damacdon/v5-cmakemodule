import ROOT
ROOT.xAOD.Init().isSuccess()
import os
files = ["/Data/DAOD_EXOT27.17882744._000026.pool.root.1"]
total = 0
for file in files:
    f = ROOT.TFile.Open(file)
    tree = ROOT.xAOD.MakeTransientMetaTree( f , "MetaData" )
    tree.GetEntry(0)
    cbks = tree.CutBookkeepers
    maxCycle = -1
    for cbk in cbks:
        if cbk.name() != "AllExecutedEvents": continue
        if cbk.inputStream() != "StreamAOD": continue
        if cbk.cycle() > maxCycle:
            maxCycle = cbk.cycle()
            theCbk = cbk
    total += theCbk.sumOfEventWeights()

print "Sum of Weights = %f " % total
ROOT.xAOD.ClearTransientTrees()
